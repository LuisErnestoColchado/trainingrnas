#include <iostream>
#include <cmath>

#define     e      (2.71828) //definimos el valor del numero e

class ActivationFunction{
	private:

		int TypeFunction; //tipo de funcion

		double MU; //variable de la funcion (Entrada efectiva (MU))

		double JaccardCoefficient;

		double Amplitude;

	public:

		ActivationFunction();

		ActivationFunction(double,int);

		void SetTypeFunction(int);

		int GetTypeFunction();

		void SetMU(double);

		double GetMU();

		double StartFunction();

		double DerivateFunction();

		void SetJaccardCoefficient(double);

		double GetJaccardCoefficient();

		void SetAmplitude(double);

		double GetAmplitude();

};

ActivationFunction::ActivationFunction(){};

ActivationFunction::ActivationFunction(double mu,int typefunction){
	TypeFunction = typefunction;
	MU = mu;
}

void ActivationFunction::SetTypeFunction(int typefunction)
{
	TypeFunction = typefunction;
}

int ActivationFunction::GetTypeFunction()
{
	return TypeFunction;
}

void ActivationFunction::SetJaccardCoefficient(double jaccardCoefficient)
{
	JaccardCoefficient = jaccardCoefficient;
}

double ActivationFunction::GetJaccardCoefficient()
{
	return JaccardCoefficient;
}

void ActivationFunction::SetAmplitude(double amplitude)
{
	Amplitude = amplitude;
}

double ActivationFunction::GetAmplitude()
{
	return Amplitude;
}

void ActivationFunction::SetMU(double mu)
{
	MU = mu;
}

double ActivationFunction::GetMU()
{
	return MU;
}

double ActivationFunction::StartFunction(){
	switch(TypeFunction)
	{
		case 1: //función Sigmoide f(x) = 1 / (1 + e ^ -MU)
			return (1.0 / (1.0 + pow(e,-MU)));
			break;
		case 2: //función Hiperbolica f(x) = (1 - e ^ -MU) / (1 + e ^ -MU)
			return (1.0 - pow(e,-MU)) / (1.0 + pow(e,-MU));
			break;
		case 3: //función Gaussiana f(x) = e ^ -(x ^ 2)/2
			return (pow(e,-1*(pow(JaccardCoefficient/Amplitude,2)/2.0)));
			break;
		case 4: //función Cuadrática f(x) = 1 / (1 + x ^ 2)
			return (1.0 / (1 + pow(JaccardCoefficient/Amplitude,2)));
			break;


	}
}

double ActivationFunction::DerivateFunction(){
	switch(TypeFunction)
	{
		case 1: //derivada de la función sigmoide f'(x) = f(x) * (1 - f(x))
			return ((1.0 / (1.0 + pow(e,-MU))) * (1 - (1.0 / (1.0 + pow(e,-MU)))));
			break;
		case 2: //derivada de la función hiperbolica f'(x) = 2 * fsigmoide(x) * (1 - fsigmoide(x))
			return (2.0 * ((1.0 / (1 + pow(e,-MU))) * (1.0 - (1.0 / (1 + pow(e,-MU))))));
			break;
		case 3: //derivada de la función gaussiana f'(x) =  e ^ (-x/2)
			return (pow(e,-1*(pow(JaccardCoefficient/Amplitude,2)/2.0))) * (pow(JaccardCoefficient,2.0)/pow(Amplitude,3.0));
			break;
		case 4: //deriva de la función cuadrática f'(x) = -2x / (1 + x ^ 2) ^ 2
			return (-2*pow(Amplitude,2.0)*JaccardCoefficient)/pow((pow(JaccardCoefficient,2.0)+pow(Amplitude,2.0)),2.0);
			break;

	}
}


