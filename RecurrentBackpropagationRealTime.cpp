/*
 * Nombre: Red recurrente y metodo de aprendizaje de Retropropagación en tiempo real(RecurrentBackpropagationRealTime.cpp)
 *
 * Autor: Luis Ernesto Colchado (agoraphobiacroz@gmail.com)
 *
 * Fecha de creación: 22 de octubre del 2017
 *
 * Fecha de la ultima modificacíon: 18 de noviembre del 2017
 *
 * Clases incluidas: Neurona(Neuron.cpp) - Conexion (Connection.cpp)
 * */



#include <iostream>
#include <cmath>
#include <sqlite3.h>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <stdlib.h>
#include "Neuron.cpp"
#include "Connection.cpp"
#include "DB.cpp"


using namespace std;

/*
 * Variable ArrayStudent
 * Arreglo bidimencional de los parametros de entrada de los 80 estudiantes.
 * Cada estudiantes tiene la siguiente información:
 *
 * cod: Codigo del estudiante
 * name: Nombre del estudiante
 * Age: Edad del estudiante
 * index1: indicador numero 1;
 * index2: indicador numero 2;
 * index3: indicador numero 3;
 * index4: indicador numero 4;
 * index5: indicador numero 5;
 * index6: indicador numero 6;
 * index7: indicador numero 7;
 * index8: indicador numero 8;
 * index9: indicador numero 9;
 * index10: indicador numero 10;
 * index11: indicador numero 11;
 * result: resultado real del estudiante
 */
Student* ArrayStudent;
const char* sql = "QueryText";

/*
 * Contador
 */
int count=0;


/*
 * Programa Principal
 */
int main(int argc, char* argv[])
{
	string textinsert="";
	/*
	 * error cometido para el alumno n
	 * y acumulador
	 * */
	double error = 0;
	double ErrorSum=0;

	/*
	 * Error total en el entrenamiento
	 * */
	double totalerror = 0;

	/*
	 * Contadores
	 */
	int n = 0;

	/*
	 * Numero de capas
	 */
	int NumberNeurons = 0;

	/*
	 * Razon de aprendizaje para la etapa de validación
	 * */
	double LearningRatio = 0.001;


	/*
	 * Varible real, para almacenar valores aleatorios
	 * */
	double random;

	/*
	 * Indicador del tipo de función. Las Funciones validas para backpropagation son:
	 * 1. Función Sigmoidal
	 * 2. Función Hiperbolica
	 * */
	int TypeFunction;
	string function;
	/*
	 * Puntero de vector tridimensional de objetos de tipo connection (Connection.cpp)
	 */
	Connection** connection;

	/*
	 * Puntero de vector bidimensional de objetos de tipo sneuron (Neuron.cpp)
	 */
	Neuron* neuron;

	/*
	 * Declaramos la variable tiempo
	 */
	int Time;
	int CountTime = 1;
	/*
	 * Puntero de vector bidimensional de los valores de errores de cada neurna
	 **/
	double** NeuronError;

	double MUTime;


	ArrayStudent = new Student[131];

	/*
	 * Leemos los parametros de la red de tipo Perceptron Multicapa y los parametros necesarios para
	 * el aprendizaje de tipo de retropropagacion para atras (backpropagation)
	 */
	cout<<"Ingrese el tiempo \n";
	cin>>Time;
	cout<<"\nTIPOS DE FUNCIÓN DE ACTIVACIÓN PARA BACKPROPAGATION:\n\n"
			"Opción 1. Función Sigmoidal"
			"\nOpción 2. Función Hiperbolica\n"
			"\nSeleccione un numero de la función que desea utilizar:\n";
	cin>>TypeFunction;
	if(TypeFunction == 1)function="sigmoide";
	else function = "hyperbolic";
	cout<<"Ingrese el numero de neuronas de contexto para la red recurrente: \n";
	cin>>NumberNeurons;
	NumberNeurons = NumberNeurons + 12;//aumentamos las 11 nueronas de entrada y la neurona de salida

	//consturimos la arquitectura
	neuron = new Neuron[NumberNeurons];
	double cache[NumberNeurons];
	connection = new Connection *[NumberNeurons];
	for(int i = 0;i<NumberNeurons;i++)
	{
		neuron[i].SetTypeFunction(TypeFunction);
		//las entrdas no son recurrentes
		connection[i] = new Connection[NumberNeurons];
		for(int j = 11;j<NumberNeurons;j++)
		{
			random = ((double)rand()/(RAND_MAX));
			connection[i][j].SetWeightValue(random);
		}
	}


	double RealTimeDerived[NumberNeurons];

	/*
	 * Salida del sistema dinamico
	 */
	double OutDinamicSystem[NumberNeurons][NumberNeurons];

	/*
	 * Acumulador
	 */
	double SumOutDinamicSystem[NumberNeurons][NumberNeurons];
	DB database;
	bool resultDB;

	resultDB = database.ConexionDB();
	if(resultDB == true)cout<<"Open Database"<<endl;
	else cout<<"Error opening Database"<<endl;

	database.SetCount(0);

	resultDB = database.GetStudents();
	if(resultDB == true){
		cout<<"Student data obtained"<<endl;
		ArrayStudent = database.GetStudentsVector();
	}
	else cout<<"Error getting student data"<<endl;

	/*
	 * Variables para cross validation
	 */
	double MSE;
	int CountCross =0;
	int Subset = 6;
	double ErrorCross;
	while(CountCross < Subset)
	{
		textinsert="";
		for(int i = 0;i<NumberNeurons;i++)
		{
			for(int j = 11;j<NumberNeurons;j++)
			{
				random = ((double)rand()/(RAND_MAX));
				connection[i][j].SetWeightValue(random);
			}
		}
		MSE =0;

		/*
		 * inicializamos el valor de las salidas de las neuronas
		 */
		for(int i = 11;i<NumberNeurons;i++)
		{
			neuron[i].SetResultActivationFunction(0);
		}

		/*
		 * Inicializamos la salida dinamica del sistema
		 */
		for(int i =0;i<NumberNeurons;i++)
		{
			for(int j =11;j<NumberNeurons;j++){

				OutDinamicSystem[i][j] = 0;
					SumOutDinamicSystem[i][j] = 0;
				}

		}
		totalerror = 0;
		n=0;
		CountTime=1;
		/*
		 * Recorremos los tiempos establecidos
		 */
		while(CountTime <= Time)
		{
			MSE=0;
			textinsert="";
			if(CountCross==0&&n==0)
			{
				n=20;
			}
			if(CountCross==1&&n==20)
			{
				n=40;
			}
			if(CountCross==2&&n==40)
			{
				n=60;
			}
			if(CountCross==3&&n==60)
			{
				n=80;
			}
			if(CountCross==4&&n==80)
			{
				n=100;
			}
			if(CountCross==5&&n==100)
			{
				cout << " tiempo " << CountTime << " error " << totalerror/n << endl;
				n=0;
				ErrorSum = 0;
			}

			/*
			 * Conseguimos el patron de entrada n
			 */
			neuron[0].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex1()));
			neuron[1].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex2()));
			neuron[2].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex3()));
			neuron[3].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex4()));
			neuron[4].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex5()));
			neuron[5].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex6()));
			neuron[6].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex7()));
			neuron[7].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex8()));
			neuron[8].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex9()));
			neuron[9].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex10()));
			neuron[10].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex11()));


			for(int i = 11;i<NumberNeurons;i++){
				MUTime =0;
				for(int j = 0;j<NumberNeurons;j++){
					MUTime = MUTime + (connection[j][i].GetWeightValue() * neuron[j].GetResultActivationFunction());
				}

				neuron[i].SetMU(MUTime);
				neuron[i].StartActivateFunction();

				if(i==NumberNeurons - 1)
				{
					error = stold(ArrayStudent[n].GetResult()) - neuron[i].GetResultActivationFunction();

				}
			}

			/*
			 * hallar sumatorias de salidas dinamica del sistema
			 */
			for(int i = 11;i < NumberNeurons;i++)
			{
				for(int j = 0;j < NumberNeurons;j++)
				{
					for(int k=11;k<NumberNeurons;k++)
					{
						SumOutDinamicSystem[j][i] = SumOutDinamicSystem[j][i] + ((connection[k][i].GetWeightValue() * OutDinamicSystem[j][i]) + neuron[j].GetResultActivationFunction());
					}
				}
			}

			/*
			 * hallar salidas dinamicas del sistema para el tiempo actual
			 */
			for(int i = 11;i < NumberNeurons;i++)
			{
				for(int j = 0;j < NumberNeurons;j++)
				{
					/*
					 * Las salidas dinamicas influyen en la actualizacion de los pesos de las conexiones
					 */
					OutDinamicSystem[j][i] = neuron[i].StartDerivateActivationFunction() * SumOutDinamicSystem[j][i];
					connection[j][i].UpdateWeightValue(LearningRatio,error,OutDinamicSystem[j][i]);
				}
			}

			error = 0.5 * pow(error,2);
			ErrorSum = ErrorSum + error;
			totalerror = ErrorSum/(n+1);

			cout << " tiempo " << CountTime << " error " << totalerror << endl;
			/*textinsert = textinsert + "insert into ResultRealTime values(" + to_string(CountTime) + ", " + to_string(totalerror) + ", "  + to_string(CountCross+1) + ", '"  + function + "');";
			sql = textinsert.c_str();
			resultDB = database.InsertResult(sql);
			if(resultDB == true)cout<<"Records created successfully"<<endl;
			else cout<<"Error while registering the date"<<endl;*

			/*
			 * Cross Validation
			 */
			int nSubset;
			int CountSubset = 0;
			cout<<" count " <<  CountCross+1 << endl;
			switch(CountCross){
				case 0:
					CountSubset = 0;
					break;
				case 1:
					CountSubset = 20;
					break;
				case 2:
					CountSubset = 40;
					break;
				case 3:
					CountSubset = 60;
					break;
				case 4:
					CountSubset = 80;
					break;
				case 5:
					CountSubset = 100;
					break;

			}
			nSubset = CountSubset + 20;
			while(CountSubset < nSubset)
			{
				//inicializamos entradas
				neuron[0].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex1()));
				neuron[1].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex2()));
				neuron[2].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex3()));
				neuron[3].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex4()));
				neuron[4].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex5()));
				neuron[5].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex6()));
				neuron[6].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex7()));
				neuron[7].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex8()));
				neuron[8].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex9()));
				neuron[9].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex10()));
				neuron[10].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex11()));

				for(int i = 11;i<NumberNeurons;i++){
					MUTime =0;
					for(int j = 0;j<NumberNeurons;j++){
						MUTime = MUTime + (connection[j][i].GetWeightValue() * neuron[j].GetResultActivationFunction());
					}
					neuron[i].SetMU(MUTime);
					cache[i] = neuron[i].GetResultActivationFunction();
					neuron[i].StartActivateFunction();
				}
				cout<< "student " << CountSubset + 1 << " salida "<<neuron[NumberNeurons - 1].GetResultActivationFunction() << endl;
				MSE = MSE + pow((stold(ArrayStudent[CountSubset].GetResult()) - neuron[NumberNeurons - 1].GetResultActivationFunction()),2);
				CountSubset++;
				for(int i = 11;i<NumberNeurons;i++){
					neuron[i].SetResultActivationFunction(cache[i]);
				}
			}
			textinsert="";
			ErrorCross = (1/20.0) * (0.5*MSE);
			textinsert = textinsert + "insert into ResultRealTime values(" + to_string(CountCross + 1) + ", " + to_string(CountTime) + ", " + to_string(totalerror) + ", " +  to_string(ErrorCross) + ", '" + function + "');";
			sql = textinsert.c_str();

			/*
			 * Invocamos a la función InsertWeightValues
			 */
			if((CountCross == 5 && n == 99) || (CountCross != 5 && n == 119) )
			{
				resultDB = database.InsertResult(sql);
				if(resultDB == true)cout<<"Records created successfully"<<endl;
				else cout<<"Error while registering the date"<<endl;
			}
			cout<<"ciclo cross " << CountCross + 1 << " error " << ErrorCross << endl;
			cout<<"Error total " << totalerror << endl;

			if(CountCross != 5 && n == 119)
			{
				n=0;
				ErrorSum = 0;
			}
			else n++;
			CountTime++;

		}
		CountCross++;
	}
}

