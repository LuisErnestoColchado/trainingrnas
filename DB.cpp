#include <iostream>
#include <stdlib.h>
#include <sqlite3.h>
#include <string>
#include <cstring>
#include <sstream>
#include "Student.cpp"
#include <vector>

using namespace std;

class DB{
	private:
		/*
		 * Variables para la conexión a base de datos
		 * */
		sqlite3* db;
		int rc;
		const char* sql = "QueryText";
		char* zErrMsg = 0;
		const char* data = "Callback function called";
		static int count;
		static Student VectorStudent[131];
	public:

		Student* GetStudentsVector();
		void SetCount(int);
		static int callback(void*,int,char**,char**);
		bool ConexionDB();
		bool GetStudents();
		bool CreateWeightValuesTable(const char*);
		bool InsertResult(const char*);

};

int DB::count = 0;
Student DB::VectorStudent[131];

Student* DB::GetStudentsVector(){
	return DB::VectorStudent;
}

void DB::SetCount(int pCount){
	DB::count = pCount;
}

/*
 * Función Callback
 * Función que recibe el regreso de los datos enviados por la base de datos SQLITE, en este
 * caso los patrones de entrada de los estudiantes
 */
  int DB::callback(void *data, int argc, char **argv, char **azColName){
   int i;
   /*for(i = 0; i<argc; i++){
	   array[i] = argv[i];
   }*/
   DB::VectorStudent[count].SetCod(argv[0]);
   DB::VectorStudent[count].SetName(argv[1]);
   DB::VectorStudent[count].SetAge(argv[2]);
   DB::VectorStudent[count].SetIndex1(argv[3]);
   DB::VectorStudent[count].SetIndex2(argv[4]);
   DB::VectorStudent[count].SetIndex3(argv[5]);
   DB::VectorStudent[count].SetIndex4(argv[6]);
   DB::VectorStudent[count].SetIndex5(argv[7]);
   DB::VectorStudent[count].SetIndex6(argv[8]);
   DB::VectorStudent[count].SetIndex7(argv[9]);
   DB::VectorStudent[count].SetIndex8(argv[10]);
   DB::VectorStudent[count].SetIndex9(argv[11]);
   DB::VectorStudent[count].SetIndex10(argv[12]);
   DB::VectorStudent[count].SetIndex11(argv[13]);
   DB::VectorStudent[count].SetResult(argv[14]);
   DB::count++;
   return 0;
}

/*
 * Función para la conexion a la base de datos
 * */
bool DB::ConexionDB(){

	zErrMsg = 0;
	rc = false;

	data = "Callback function called";

	/* Open database */
	rc = sqlite3_open("/home/luis-ernesto/trainingrnas/dbtrainingrnas", &db);
	if (rc)
	{
		return false;
	}
	else
	{
		return true;
	}
}

/*
 * Función para conseguir datos los estudiantes (invoca a la funcion callback)
 */
bool DB::GetStudents(){

	sql = "SELECT * from student";
	rc = sqlite3_exec(db, sql, DB::callback, (void*)data, &zErrMsg);

	if( rc != SQLITE_OK ) {
		return false;
	}else {
		return true;
	}
}

/*
 * Función para crear la tabla de pesos adecuados, obtenidos al final del entrenamiento
 */

bool DB::CreateWeightValuesTable(const char* psql){

	/* Execute SQL statement */
	rc = sqlite3_exec(db, psql, NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
 * Función para insertar los pesos a la tabla de pesos adecuados creada en la base de datos
 */
bool DB::InsertResult(const char* psql){
	rc = sqlite3_exec(db, psql, NULL, 0, &zErrMsg);

	if( rc != SQLITE_OK ){
		return false;
	} else {
	    return true;
	}
	sqlite3_close(db);
}
