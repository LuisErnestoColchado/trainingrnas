/*
 * Nombre: Red de base radial y metodo de aprendizaje Hibrido (RadialBaseHibryd.cpp)
 *
 * Autor: Luis Ernesto Colchado (agoraphobiacroz@gmail.com)
 *
 * Fecha de creación: 15 de octubre del 2017
 *
 * Fecha de la ultima modificacíon: 17 de noviembre del 2017
 *
 * Clases incluidas: Neurona(Neuron.cpp) - Conexion (Connection.cpp)
 */

#include <iostream>
#include <cmath>
#include <sqlite3.h>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <stdlib.h>
#include "Neuron.cpp"
#include "Connection.cpp"
#include "DB.cpp"

using namespace std;



int main()
{
	/*
	 * Variable ArrayStudent
	 * Arreglo bidimencional de los parametros de entrada de los 80 estudiantes.
	 * Cada estudiantes tiene la siguiente información:
	 *
	 * cod: Codigo del estudiante
	 * name: Nombre del estudiante
	 * Age: Edad del estudiante
	 * index1: indicador numero 1;
	 * index2: indicador numero 2;
	 * index3: indicador numero 3;
	 * index4: indicador numero 4;
	 * index5: indicador numero 5;
	 * index6: indicador numero 6;
	 * index7: indicador numero 7;
	 * index8: indicador numero 8;
	 * index9: indicador numero 9;
	 * index10: indicador numero 10;
	 * index11: indicador numero 11;
	 * result: resultado real del estudiante
	 */
	Student* ArrayStudent;
	Student* StudentModes;

	const char* sql = "QueryText";
	/*
	 * error cometido para el alumno n
	 * y acumulador
	 * */
	double error = 0;
	double ErrorSum=0;

	/*
	 * Error total en el entrenamiento
	 * */
	double totalerror = 1;

	int NumberNeuronsForHiddenLayer;

	/*
	 * Contadores
	 */
	int n = 0;
	int count=0;

	/*
	 * Razon de aprendizaje para la etapa de validación
	 * */
	double LearningRatio =0.0001;

	/*
	 * Numero de ciclos de aprendizaje para cada estudiante n
	 * Contador de ciclos de aprendizaje
	 * */
	double LearningCycle;
	double CountLearningCycle=1;


	/*
	 * Varible para almacenar valores aleatorios
	 * */
	int random;

	/*
	 * Indicador del tipo de función. Las Funciones validas para base radial son:
	 * 1. Función Gausssiana
	 * 2. Función Inversa cuadratica
	 * */
	int TypeFunction;

	/*
	 * Puntero de vector tridimensional de objetos de tipo connection (Connection.cpp)
	 */
	Connection** connection;

	/*
	 * Puntero de vector bidimensional de objetos de tipo neuron (Neuron.cpp)
	 */
	Neuron** neuron;

	/*
	 * Puntero de vector bidimensional de los valores de errores de cada neurna
	 **/
	double** NeuronError;

	string function;
	/*
	 * Leemos los parametros de la red de tipo Base Radial y los parametros necesarios para
	 * el aprendizaje de tipo hibrido
	 */
	cout<<"Ingrese el numero de ciclos\n";
	cin>>LearningCycle;
	cout<<"\nTIPOS DE FUNCIÓN DE ACTIVACIÓN PARA Metodo Hibrido:\n\n"
			"Opción 3. Función Gaussiana\n"
			"Opción 4. Función Inversa Cuadratica"
			"\nSeleccione un numero de la funcion que desea utilizar:\n";
	cin>>TypeFunction;
	if(TypeFunction==3)function = "gaussian";
	else function = "cuadratica";
	cout<<"Ingrese el numero de neuronas en la capa oculta";
	cin>>NumberNeuronsForHiddenLayer;

	/*
	 * los centros de cada neurona ocuta es una clase
	 */
	double centers[NumberNeuronsForHiddenLayer];


	StudentModes = new Student[100];

	/*
	 * creamos la arquitectura de las red de base radial
	 */
	neuron = new Neuron *[3];
	neuron[0] = new Neuron[11];
	neuron[1] = new Neuron[NumberNeuronsForHiddenLayer];
	neuron[2] = new Neuron[1];

	/*
	 * Creamos las conexion de la red
	 */
	connection = new Connection *[2];
	connection[0] = new Connection[11];
	connection[1] = new Connection[NumberNeuronsForHiddenLayer];

	/*
	 * Seleccionamos el tipo de funcion en las neuronas
	 */
	for(int i =0;i < 11;i++)
	{
		neuron[0][i].SetIsInput(true);
		neuron[0][i].SetTypeFunction(TypeFunction);
	}

	for(int j = 0;j < NumberNeuronsForHiddenLayer;j++)
	{
		neuron[1][j].SetIsInput(false);
		neuron[1][j].SetTypeFunction(TypeFunction);
	}

	neuron[2][0].SetIsInput(false);
	neuron[2][0].SetTypeFunction(TypeFunction);

	DB database;
	bool resultDB;

	resultDB = database.ConexionDB();
	if(resultDB == true)cout<<"Open Database"<<endl;
	else cout<<"Error opening Database"<<endl;

	database.SetCount(0);

	resultDB = database.GetStudents();
	if(resultDB == true){
			cout<<"Student data obtained"<<endl;
			ArrayStudent = database.GetStudentsVector();
	}
	else cout<<"Error getting student data"<<endl;


	/*
	 * Acumulador para la sumatoria de la entrada efectiva de cada neurona
	 */
	double auxmu=0;

	string textinsert="";

	/*
	 * Arreglo de las modas de los centros de las neuronas
	 */
	string modes[NumberNeuronsForHiddenLayer][11];

	double RandomForWeight;

	/*
	 * Variable error medio cuadratico
	 */
	double MSE;

	/*
	 * Variables para Cross validation
	 */
	int CountCross =0;
	int Subset = 6;
	double ErrorCross;

	/*
	 * variables para determinar las modas de los centros de las neuronas
	 */
	int aux=0;
	int k=0;
	int p=0;
	double q=0,r=0,s=0,t=0;
	double menor;
	string OldModes[NumberNeuronsForHiddenLayer][11];
	bool indicador = false;

	int ClassError[NumberNeuronsForHiddenLayer];

	/*
	 * Variables para el calculo jaccard
	 */
	int AtributeFrecuencyTrue[NumberNeuronsForHiddenLayer][11];
	int AtributeFrecuencyFalse[NumberNeuronsForHiddenLayer][11];
	double CoeficityJaccard[NumberNeuronsForHiddenLayer];
	double CoeficityJaccard1[NumberNeuronsForHiddenLayer][NumberNeuronsForHiddenLayer];

	/*
	 * Variables para calcular las amplitudes entre los centros de las neuronas de la capa oculta
	 */
	double media[NumberNeuronsForHiddenLayer];
	double amplitude[NumberNeuronsForHiddenLayer];

	double sum;
	double array[NumberNeuronsForHiddenLayer -1];
	/*
	 * Iniciamos el recorrido por los bloques de cross validation (son seis veces)
	 */
	while(CountCross<Subset)
	{
		aux=0;
		k=0;
		p=0;
		while(k<120)
		{
			 if(CountCross==0&&k==0)
			 {
				 k=20;
			 }
			 if(CountCross==1&&k==20)
			 {
				 k=40;
			 }
			 if(CountCross==2&&k==40)
			 {
				 k=60;
			 }
			 if(CountCross==3&&k==60)
			 {
				 k=80;
			 }
			 if(CountCross==4&&k==80)
			 {
				 k=100;
			 }
			 if(CountCross==5&&k==100)
			 {
				 break;
			 }
			 /*
			  * Seleccionamos el conjunto de patrones para aprendizaje en el actual reccorido de Cross validation
			  */
			StudentModes[p].SetIndex1(ArrayStudent[k].GetIndex1());
			StudentModes[p].SetIndex2(ArrayStudent[k].GetIndex2());
			StudentModes[p].SetIndex3(ArrayStudent[k].GetIndex3());
			StudentModes[p].SetIndex4(ArrayStudent[k].GetIndex4());
			StudentModes[p].SetIndex5(ArrayStudent[k].GetIndex5());
			StudentModes[p].SetIndex6(ArrayStudent[k].GetIndex6());
			StudentModes[p].SetIndex7(ArrayStudent[k].GetIndex7());
			StudentModes[p].SetIndex8(ArrayStudent[k].GetIndex8());
			StudentModes[p].SetIndex9(ArrayStudent[k].GetIndex9());
			StudentModes[p].SetIndex10(ArrayStudent[k].GetIndex10());
			k++;
			p++;
		}
	/*
	 * se elige un patron  para cada clase, aleatoriamente
	 */
	random = 100/NumberNeuronsForHiddenLayer;
	for(int i =0;i <  NumberNeuronsForHiddenLayer; i++)
	{
		modes[i][0] = StudentModes[aux].GetIndex1();
		modes[i][1] = StudentModes[aux].GetIndex2();
		modes[i][2] = StudentModes[aux].GetIndex3();
		modes[i][3] = StudentModes[aux].GetIndex4();
		modes[i][4] = StudentModes[aux].GetIndex5();
		modes[i][5] = StudentModes[aux].GetIndex6();
		modes[i][6] = StudentModes[aux].GetIndex7();
		modes[i][7] = StudentModes[aux].GetIndex8();
		modes[i][8] = StudentModes[aux].GetIndex9();
		modes[i][9] = StudentModes[aux].GetIndex10();
		modes[i][10] = StudentModes[aux].GetIndex11();
		aux = aux + random;
	}

	string CurrentStudent[11];

	q=0;r=0;s=0;t=0;

	indicador = false;

	/*
	 * Actualizamos las modas de los centros de cada neurona hasta que estas no varien respecto a su anterior
	 */
	while(indicador==false)
	{
		n=0;
		while(n < 100)
		{
			CurrentStudent[0] = StudentModes[n].GetIndex1();
			CurrentStudent[1] = StudentModes[n].GetIndex2();
			CurrentStudent[2] = StudentModes[n].GetIndex3();
			CurrentStudent[3] = StudentModes[n].GetIndex4();
			CurrentStudent[4] = StudentModes[n].GetIndex5();
			CurrentStudent[5] = StudentModes[n].GetIndex6();
			CurrentStudent[6] = StudentModes[n].GetIndex7();
			CurrentStudent[7] = StudentModes[n].GetIndex8();
			CurrentStudent[8] = StudentModes[n].GetIndex9();
			CurrentStudent[9] = StudentModes[n].GetIndex10();
			CurrentStudent[10] = StudentModes[n].GetIndex11();

			menor=12;

			/*
			 * calculamos y seleccionamos el centro con el cual tiene menor error el actual patron
			 */
			for(int i = 0;i<NumberNeuronsForHiddenLayer;i++)
			{
				ClassError[i]=0;
				for(int j = 0;j<11;j++)
				{
					if(CurrentStudent[j] != modes[i][j])
					{
						ClassError[i]++;
					}
				}
				if(ClassError[i] < menor){
					menor = ClassError[i];
					StudentModes[n].SetCluster(to_string(i+1));
				}
			}
			cout << "student " << n+1 << " cluster" << StudentModes[n].GetCluster() <<endl;
			n++;
		}

		/*
		 * inicializamos contadores
		 */
		for(int i =0;i<NumberNeuronsForHiddenLayer;i++)
		{
			for(int j = 0;j<11;j++)
			{

					AtributeFrecuencyTrue[i][j] =0;
					AtributeFrecuencyFalse[i][j] =0;
			}
		}

		/*
		 * recalculamos las modas
		 */
		for(int i =0;i<NumberNeuronsForHiddenLayer;i++)
		{

			for(int k =0;k<11;k++)
			{
				OldModes[i][k] = modes[i][k];
			}
			n=0;
			while(n<100)
			{
				if(StudentModes[n].GetCluster() == to_string(i+1))
				{
					CurrentStudent[0] = StudentModes[n].GetIndex1();
					CurrentStudent[1] = StudentModes[n].GetIndex2();
					CurrentStudent[2] = StudentModes[n].GetIndex3();
					CurrentStudent[3] = StudentModes[n].GetIndex4();
					CurrentStudent[4] = StudentModes[n].GetIndex5();
					CurrentStudent[5] = StudentModes[n].GetIndex6();
					CurrentStudent[6] = StudentModes[n].GetIndex7();
					CurrentStudent[7] = StudentModes[n].GetIndex8();
					CurrentStudent[8] = StudentModes[n].GetIndex9();
					CurrentStudent[9] = StudentModes[n].GetIndex10();
					CurrentStudent[10] = StudentModes[n].GetIndex11();
					for(int j = 0;j<11;j++)
					{
						if(CurrentStudent[j] == "1")
							AtributeFrecuencyTrue[i][j]++;
						else
							AtributeFrecuencyFalse[i][j]++;
					}
				}

				n++;
			}
		}
		for(int i = 0;i<NumberNeuronsForHiddenLayer;i++)
		{
			for(int j = 0;j < 11;j++)
			{
				if(AtributeFrecuencyTrue[i][j] > AtributeFrecuencyFalse[i][j])
				{
					modes[i][j] = "1";
				}
				else{
					modes[i][j] = "0";
				}
				AtributeFrecuencyTrue[i][j] = 0;
				AtributeFrecuencyFalse[i][j] = 0;
			}
		}

		for(int i = 0;i<NumberNeuronsForHiddenLayer;i++)
		{
			for(int j = 0;j < 11;j++)
			{
				if(modes[i][j] == OldModes[i][j])
				{
					/*
					 * Si la anterior moda calculada es igual a la actual
					 */
					indicador=true;
				}
				else{
					/*
					 * Caso contrario
					 */
					indicador = false;
					break;
				}
			}
			if(indicador == false)break;
		}

			cout<<"indicador " << indicador << endl;
			if(indicador == true)break;
		}

		for(int i =0;i<NumberNeuronsForHiddenLayer;i++)
		{
			for(int k =0;k<11;k++)
			{
				cout<< " moda " << i <<  " valor " << modes[i][k] << endl;
			}
		}

		/*
		 * Calculamos las amplitudes
		 */
		for(int i = 0;i<NumberNeuronsForHiddenLayer;i++)
		{

			sum =0;
			for(int j = 0;j<NumberNeuronsForHiddenLayer;j++)
			{

				q=0;
				r=0;
				s=0;
				t=0;
				if(i!=j)
				{
					for(int k =0;k<11;k++)
					{
						if(modes[i][k] == "1" && modes[j][k] == "1")q++;
						if(modes[i][k] == "1" && modes[j][k] == "0")r++;
						if(modes[i][k] == "0" && modes[j][k] == "1")s++;
					}
					CoeficityJaccard1[i][j] = ((r+s)/(q+r+s));
					cout << i << j << " coeficity"  << CoeficityJaccard1[i][j] << endl;
					sum = sum + CoeficityJaccard1[i][j];
				}

			}
			count=0;
			for(int j = 0;j<NumberNeuronsForHiddenLayer;j++)
			{
				if(i!=j)
				{
					array[count] = CoeficityJaccard1[i][j];
					count++;
				}
			}

			if(NumberNeuronsForHiddenLayer!=2)
			{
			double AuxJaccard;
			 for (int i = 0; i < count - 1; i++) {
					for (int x = i + 1; x < count; x++) {
						if (array[x] < array[i]) {
							AuxJaccard = array[i];
							array[i] = array[x];
							array[x] = AuxJaccard;
						}
					}
			 }
				sum = array[0] * array[1];

			}


			amplitude[i] = pow(sum,0.5);
			cout << "suma " << amplitude[i] << endl;
		}

		MSE =0;
		CountLearningCycle =1;
		for(int i = 0;i<NumberNeuronsForHiddenLayer;i++){
			RandomForWeight = ((double)rand()/(RAND_MAX));
				/*
				 * inicializamos los pesos de las conexiones
				 */
				connection[1][i].SetWeightValue(RandomForWeight);
		}
		LearningRatio =0.0001;

		while(CountLearningCycle <= LearningCycle)
		{
			textinsert="";
			ErrorSum = 0;
			MSE=0;
			n=0;

			while(n<120)
			{
				 if(CountCross==0&&n==0)
				 {
					 n=20;
				 }
				 if(CountCross==1&&n==20)
				 {
					 n=40;
				 }
				 if(CountCross==2&&n==40)
				 {
					 n=60;
				 }
				 if(CountCross==3&&n==60)
				 {
					 n=80;
				 }
				 if(CountCross==4&&n==80)
				 {
					 n=100;
				 }
				 if(CountCross==5&&n==100)
				 {
					 break;
				 }

				/*
				 * Conseguimos los datos del patron n
				 */
				neuron[0][0].SetResultActivationFunction(stod(ArrayStudent[n].GetIndex1()));
				neuron[0][1].SetResultActivationFunction(stod(ArrayStudent[n].GetIndex2()));
				neuron[0][2].SetResultActivationFunction(stod(ArrayStudent[n].GetIndex3()));
				neuron[0][3].SetResultActivationFunction(stod(ArrayStudent[n].GetIndex4()));
				neuron[0][4].SetResultActivationFunction(stod(ArrayStudent[n].GetIndex5()));
				neuron[0][5].SetResultActivationFunction(stod(ArrayStudent[n].GetIndex6()));
				neuron[0][6].SetResultActivationFunction(stod(ArrayStudent[n].GetIndex7()));
				neuron[0][7].SetResultActivationFunction(stod(ArrayStudent[n].GetIndex8()));
				neuron[0][8].SetResultActivationFunction(stod(ArrayStudent[n].GetIndex9()));
				neuron[0][9].SetResultActivationFunction(stod(ArrayStudent[n].GetIndex10()));
				neuron[0][10].SetResultActivationFunction(stod(ArrayStudent[n].GetIndex11()));

				for(int i = 0;i<NumberNeuronsForHiddenLayer;i++)
				{
					/*
					 * Calculamos la difencia entre el patron ingresado y el centro de la neurona
					 */
					for(int j = 0;j<11;j++)
					{
						if(neuron[0][j].GetResultActivationFunction() == 1.0 && modes[i][j] == "1")q++;
						if(neuron[0][j].GetResultActivationFunction() == 1.0 && modes[i][j] == "0")r++;
						if(neuron[0][j].GetResultActivationFunction() == 0 && modes[i][j] == "1")s++;
					}
					CoeficityJaccard[i] = ((r+s)/(q+r+s));
					neuron[1][i].SetJaccardCoefficient(CoeficityJaccard[i]);
					neuron[1][i].SetAmplitude(amplitude[i]);
					neuron[1][i].StartActivateFunction();
					q=0;r=0;s=0;
				}

				/*
				 * Actualizamos los pesos mediante el metodo de minimos cuadrados
				 */
				sum=0;
				for(int i = 0;i<NumberNeuronsForHiddenLayer;i++)
				{
					sum = sum + (neuron[1][i].GetResultActivationFunction() * connection[1][i].GetWeightValue());
				}
				neuron[2][0].SetResultActivationFunction(sum);

				error = stold(ArrayStudent[n].GetResult()) - neuron[2][0].GetResultActivationFunction();

				for(int i =0;i<NumberNeuronsForHiddenLayer;i++)
				{
					connection[1][i].UpdateWeightValue(LearningRatio,neuron[1][i].GetResultActivationFunction(),error);
				}

				ErrorSum = ErrorSum + (pow(error,2.0));
				n++;
			}
			totalerror = (1.0/100.0) * (0.5*ErrorSum);
			cout<< " ciclo "<< CountLearningCycle << " error " << totalerror << endl;

			textinsert="";
			/*
			 * Cross Validation
			 */
			int nSubset;
			int CountSubset = 0;
			cout<<" count " <<  CountCross+1 << endl;
			switch(CountCross){
					case 0:
						CountSubset = 0;
						break;
					case 1:
						CountSubset = 20;
						break;
					case 2:
						CountSubset = 40;
						break;
					case 3:
						CountSubset = 60;
						break;
					case 4:
						CountSubset = 80;
						break;
					case 5:
						CountSubset = 100;
						break;
			}
			nSubset = CountSubset + 20;
			while(CountSubset < nSubset)
			{
				/*
				 * conseguimos datos para validacion
				 */
				neuron[0][0].SetResultActivationFunction(stod(ArrayStudent[CountSubset].GetIndex1()));
				neuron[0][1].SetResultActivationFunction(stod(ArrayStudent[CountSubset].GetIndex2()));
				neuron[0][2].SetResultActivationFunction(stod(ArrayStudent[CountSubset].GetIndex3()));
				neuron[0][3].SetResultActivationFunction(stod(ArrayStudent[CountSubset].GetIndex4()));
				neuron[0][4].SetResultActivationFunction(stod(ArrayStudent[CountSubset].GetIndex5()));
				neuron[0][5].SetResultActivationFunction(stod(ArrayStudent[CountSubset].GetIndex6()));
				neuron[0][6].SetResultActivationFunction(stod(ArrayStudent[CountSubset].GetIndex7()));
				neuron[0][7].SetResultActivationFunction(stod(ArrayStudent[CountSubset].GetIndex8()));
				neuron[0][8].SetResultActivationFunction(stod(ArrayStudent[CountSubset].GetIndex9()));
				neuron[0][9].SetResultActivationFunction(stod(ArrayStudent[CountSubset].GetIndex10()));
				neuron[0][10].SetResultActivationFunction(stod(ArrayStudent[CountSubset].GetIndex11()));
				for(int i = 0;i<NumberNeuronsForHiddenLayer;i++)
				{
					for(int j = 0;j<11;j++)
					{
						if(neuron[0][j].GetResultActivationFunction() == 1.0 && modes[i][j] == "1")q++;
						if(neuron[0][j].GetResultActivationFunction() == 1.0 && modes[i][j] == "0")r++;
						if(neuron[0][j].GetResultActivationFunction() == 0 && modes[i][j] == "1")s++;
					}
					CoeficityJaccard[i] = ((r+s)/(q+r+s));
					neuron[1][i].SetJaccardCoefficient(CoeficityJaccard[i]);
					neuron[1][i].SetAmplitude(amplitude[i]);
					neuron[1][i].StartActivateFunction();
					q=0;r=0;s=0;
				}

				sum=0.0;
				for(int i = 0;i<NumberNeuronsForHiddenLayer;i++)
				{
					sum = sum + (neuron[1][i].GetResultActivationFunction() * connection[1][i].GetWeightValue());
				}
				neuron[2][0].SetResultActivationFunction(sum);
				/*
				 * sumatoria para el error medio cuadratico para el patron actual
				 */
				MSE = MSE + pow((stold(ArrayStudent[CountSubset].GetResult()) - neuron[2][0].GetResultActivationFunction()),2);
				cout << " student " << CountSubset+1 << " out " << neuron[2][0].GetResultActivationFunction() << endl;
				CountSubset++;
			}
			/*
			 * Calculamso el error cuadratico medio
			 */
			ErrorCross = (1/20.0) * (0.5 * MSE);
			textinsert = "";
			textinsert = textinsert + "insert into ResultHibryd values(" + to_string(CountCross + 1) + ", "  + to_string(CountLearningCycle) + ", " + to_string(totalerror) + ", " + to_string(ErrorCross)  + ", '" + function  + "');";
			sql = textinsert.c_str();
			/*
			 * Invocamos a la función InsertWeightValues
			 */
			resultDB = database.InsertResult(sql);
			if(resultDB == true)cout<<"Records created successfully"<<endl;
			else cout<<"Error while registering the date"<<endl;
			cout<<"************************************************************"<<endl;
			cout<< "MSE Cross" << CountCross+1  << " error " << ErrorCross << endl;
			cout<<"************************************************************"<<endl;
			LearningRatio = LearningRatio + 0.0001;
			CountLearningCycle++;
		}
		CountCross++;
	}
}


