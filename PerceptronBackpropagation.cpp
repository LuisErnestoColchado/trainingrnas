/*
 * Nombre: Perceptron multica y metodo de aprendizaje de Retropropagación (PerceptronBackpropagation.cpp)
 *
 * Autor: Luis Ernesto Colchado (agoraphobiacroz@gmail.com)
 *
 * Fecha de creación: 10 de octubre del 2017
 *
 * Fecha de la ultima modificacíon: 16 de noviembre del 2017
 *
 * Clases incluidas: Neurona(Neuron.cpp) - Conexion (Connection.cpp)
 */


#include <iostream>
#include <cmath>
#include <sqlite3.h>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <stdlib.h>
#include "Neuron.cpp"
#include "Connection.cpp"
#include "DB.cpp"


using namespace std; 

/*
 * Variable ArrayStudent
 * Arreglo bidimencional de los parametros de entrada de los 283 estudiantes.
 * Cada estudiantes tiene la siguiente información:
 *
 * cod: Codigo del estudiante
 * name: Nombre del estudiante
 * Age: Edad del estudiante
 * index1: indicador numero 1;
 * index2: indicador numero 2;
 * index3: indicador numero 3;
 * index4: indicador numero 4;
 * index5: indicador numero 5;
 * index6: indicador numero 6;
 * index7: indicador numero 7;
 * index8: indicador numero 8;
 * index9: indicador numero 9;
 * index10: indicador numero 10;
 * index11: indicador numero 11;
 * result: resultado real del estudiante
 */
Student* ArrayStudent;
const char* sql = "QueryText";

/*
 * Contador
 */
int count=0;


/*
 * Programa Principal
 */
int main(int argc, char* argv[])
{
	/*
	 * error cometido para el alu
	 * mno n
	 * y acumulador
	 * */
	double error = 0;
	double ErrorSum=0;

	/*
	 * Error total en el entrenamiento
	 * */
	double totalerror = 1;

	/*
	 * Contador (indicador del patron actual)
	 */
	int n = 0;
	
	/*
	 * Numero de capas
	 */
	int NumberLayer = 0;
	
	/*
	 * Razon de aprendizaje para la etapa de validación
	 * */
	double LearningRatio = 0.00001;
	
	/*
	 * Numero de ciclos de aprendizaje para cada estudiante n
	 * Contador de ciclos de aprendizaje
	 * */
	double LearningCycle;
	double CountLearningCycle=1;

	stringstream NumberString;

	/*
	 * Varible real, para almacenar valores aleatorios
	 * */
	double random;

	/*
	 * Indicador del tipo de función. Las Funciones validas para backpropagation son:
	 * 1. Función Sigmoidal
	 * 2. Función Hiperbolica
	 * */
	int TypeFunction;
	string function;
	
	/*
	 * Puntero de vector tridimensional de objetos de tipo connection (Connection.cpp)
	 */
	Connection*** connection;

	/*
	 * Puntero de vector bidimensional de objetos de tipo neuron (Neuron.cpp)
	 */
	Neuron** neuron;

	/*
	 * Puntero de vector bidimensional de los valores de errores de cada neurona en la capa oculta
	 */
	double** NeuronError;

	/*Establecemos el numero de patrones*/
	ArrayStudent = new Student[131];

	/*
	 * Leemos los parametros de la red de tipo Perceptron Multicapa y los parametros necesarios para
	 * el aprendizaje de tipo de retropropagacion para atras (backpropagation)
	 */
	cout<<"Ingrese el numero de ciclos de aprendizaje \n";
	cin>>LearningCycle;
	cout<<"\nTIPOS DE FUNCIÓN DE ACTIVACIÓN PARA BACKPROPAGATION:\n\n"
			"Opción 1. Función Sigmoidal"
			"\nOpción 2. Función Hiperbolica\n"
			"\nSeleccione un numero de la función que desea utilizar:\n";
	cin>>TypeFunction;
	if(TypeFunction == 1)function="sigmoide";
	else function = "hyperbolic";
	cout<<function<<endl;
	cout<<"Ingrese el numero de capas ocultas: \n";
	cin>>NumberLayer;
	NumberLayer = NumberLayer + 2;
	int NumberNeuronsForLayer[NumberLayer];
	for(int i = 0;i < NumberLayer;i++)
	{
		if(i == 0)	NumberNeuronsForLayer[i] = 11;
		else if(i == NumberLayer - 1) NumberNeuronsForLayer[i] = 1;
		else{
			NumberString<<(i);
			cout<<"Ingrese el numero de neuronas en la capa oculta numero N°" + NumberString.str() + "\n";
			NumberString.str("");
			cin>>NumberNeuronsForLayer[i];
		}
	}

	/*
	 * Creamos la arquitectura de la red segun los datos ingresados anteriormente
	 */
	neuron = new Neuron *[NumberLayer];
	connection = new Connection **[NumberLayer];
	NeuronError = new double *[NumberLayer];
	for(int i=0;i<NumberLayer;i++)
	{
		neuron[i] = new Neuron[NumberNeuronsForLayer[i]];
		connection[i] = new Connection *[NumberNeuronsForLayer[i]];
		NeuronError[i] = new double[NumberNeuronsForLayer[i]];
		for(int j = 0;j<NumberNeuronsForLayer[i];j++)
		{
			if(i < NumberLayer -1)
				connection[i][j] = new Connection[NumberNeuronsForLayer[i+1]];
		}

	}

	/*
	 * Realizamos el recorrido por cada una de las neuronas para establecer
	 * el numero de capa y el tipo de funcion en cada neurona y ademas
	 * inicializamos los pesos de las conexiones con valores aleatorios
	 */
	for(int j = 0;j < NumberLayer;j++)
	{
		for(int k = 0;k < NumberNeuronsForLayer[j];k++)
		{
			neuron[j][k].SetLayer(j);
			string s = to_string(j) + to_string(k);
						cout<<s + "\n";
			if(j == 0){
				/*
				 * Seleccionamos el tipo de función en cada neurona
				 * además, indicamos que estas neuronas son de la capa de entrada
				 */
				neuron[j][k].SetTypeFunction(TypeFunction);
				neuron[j][k].SetIsInput(true);
			}
			else{
				/*
				 * También seleccionamos el tipo de función
				 * en este caso indicamos que estas neuronas no son de la capa de entrada
				 */
				neuron[j][k].SetTypeFunction(TypeFunction);
				neuron[j][k].SetIsInput(false);
				random = ((double)rand()/(RAND_MAX));
				neuron[j][k].SetBias(random);
			}

			/*
			 * Inicializamos los pesos de las conexiones con valores aletorios entre 0 - 1
			 */
			if(j < NumberLayer - 1)
			{
				for(int g = 0;g < NumberNeuronsForLayer[j+1];g++)
				{
					random = ((double)rand()/(RAND_MAX));
					connection[j][k][g].SetWeightValue(random);
					cout<<connection[j][k][g].GetWeightValue()<<endl;
				}
			}
			count++;
		}
	}

	/*
	 * Llamamos a la clase DB para realizar la conexion a la base de datos y la consulta
	 * para obtener los datos de los patrones de entrenamiento
	 */
	DB database;
	bool resultDB;

	resultDB = database.ConexionDB();
	if(resultDB == true)cout<<"Open Database"<<endl;
	else cout<<"Error opening Database"<<endl;
	database.SetCount(0);

	resultDB = database.GetStudents();
	if(resultDB == true){
		cout<<"Student data obtained" << endl;
		ArrayStudent = database.GetStudentsVector();
	}
	else cout<<"Error getting student data"<<endl;

	/*
	 * Acumulador para la sumatoria de la entrada efectiva de cada neurona
	 */
	double auxmu=0;

	string textinsert="";

	/*
	 *	Recorremos los seis bloques de cros validation
	 */
	int Subset = 6;
	int CountCross=0;
	double MSE = 0;
	double ErrorCross;


	while(CountCross<Subset)
	{

		LearningRatio = 0.00001;

		CountLearningCycle=1;
		for(int j = 0;j < NumberLayer;j++)
		{
			for(int k = 0;k < NumberNeuronsForLayer[j];k++)
			{

				if(j!=0){

					/*
					 * También seleccionamos el tipo de función
					 * en este caso indicamos que estas neuronas no son de la capa de entrada
					 */
					random = ((double)rand()/(RAND_MAX));
					neuron[j][k].SetBias(random);
				}
				if(j < NumberLayer - 1)
				{
					for(int g = 0;g < NumberNeuronsForLayer[j+1];g++)
					{
						random = ((double)rand()/(RAND_MAX));
						connection[j][k][g].SetWeightValue(random);
					}
				}
			}
		}
		 /*
		  * Recorremos el numero de ciclos de aprendizaje establecidos
		  */


		 while(CountLearningCycle <= LearningCycle)
		 {
			 MSE=0;
			 n=0;
			 error=0;
				textinsert = "";

			 /*
			  * Realizamos el recorrido por los patrones
			  */
			 while(n < 120)
			 {

				 if(CountCross==0&&n==0)
				 {
					 n=20;
				 }
				 if(CountCross==1&&n==20)
				 {
					 n=40;
				 }
				 if(CountCross==2&&n==40)
				 {
					 n=60;
				 }
				 if(CountCross==3&&n==60)
				 {
					 n=80;
				 }
				 if(CountCross==4&&n==80)
				 {
					 n=100;
				 }
				 if(CountCross==5&&n==100)
				 {
					 break;
				 }
				 auxmu=0;
				 /*
				  * Las neuronas de entrada no tiene una función de activación por ello su resultado son los valores
				  * recolectados de los estudiantes.
				  */
				 neuron[0][0].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex1()));
				 neuron[0][1].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex2()));
				 neuron[0][2].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex3()));
				 neuron[0][3].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex4()));
				 neuron[0][4].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex5()));
				 neuron[0][5].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex6()));
				 neuron[0][6].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex7()));
				 neuron[0][7].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex8()));
				 neuron[0][8].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex9()));
				 neuron[0][9].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex10()));
				 neuron[0][10].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex11()));

				 for(int i =1;i < NumberLayer;i++)
				 {
					for(int k = 0;k < NumberNeuronsForLayer[i];k++)
					{
						auxmu =0;
						/*
						 * Esta condicional realiza la inspeccion si en el recorrido i, se trata de una neuorna de la
						 * capa oculta numero 1, en este caso esta neurona esta conectada a la capa anterior por medio
						 * de las conexiones de la capa de entrada
						 */
						if(i==1)
						{

							for(int l = 0;l < 11;l++)
							{
								/*
								 * Sumatoria de entrada efectiva de cada neurona
								 */
								auxmu = auxmu + (neuron[0][l].GetResultActivationFunction()
											*  connection[0][l][k].GetWeightValue());
							}
							auxmu = auxmu + neuron[i][k].GetBias();

							/*
							 * Establecemos la sumatoria de entrada efectiva a su respectiva neurona
							 */
							neuron[i][k].SetMU(auxmu);

							/*
							 * Invocamos a la función, que operara la función de activación en la neurona
							 * con la entrada efectiva anteriormente establecida
							 */
							neuron[i][k].StartActivateFunction();
						}
						else
						{
							for(int j = 0;j < NumberNeuronsForLayer[i-1];j++)
							{
								/*
								 * Sumatoria de entrada efectiva de cada neurona
								 */
								auxmu = auxmu + (neuron[i-1][j].GetResultActivationFunction() *
										connection[i-1][j][k].GetWeightValue());

							}

							auxmu = auxmu + neuron[i][k].GetBias();
							/*
							 * Establecemos la sumatoria de entrada efectiva a su respectiva neurona
							 */
							neuron[i][k].SetMU(auxmu);

							/*
							 * Invocamos a la función, que operara la función de activación en la neurona
							 * con la entrada efectiva anteriormente establecida
							 */
							neuron[i][k].StartActivateFunction();
						}

					}

				}

				/*
				 * Calculamos el error cometido por la neurona de salida
				 */
				 NeuronError[NumberLayer - 1][0] = (stold(ArrayStudent[n].GetResult()) - neuron[NumberLayer - 1][0].GetResultActivationFunction());

				/*
				 * Realizamos la retropropagación del error hacia todas las neuronas de las capas anteriores
				 */
				for(int i = NumberLayer - 2;i >= 1;i--){
					 for(int j = 0;j < NumberNeuronsForLayer[i];j++){
						 ErrorSum = 0;
						 for(int k = 0;k < NumberNeuronsForLayer[i+1];k++){
							 ErrorSum = ErrorSum + (neuron[i][j].GetResultActivationFunction() * connection[i][j][k].GetWeightValue());

						 }
						 NeuronError[i][j] = neuron[i][j].StartDerivateActivationFunction() * ErrorSum;
					 }
				 }

				 /*
				  * Actualizamos los pesos de las conexiones en base a los errores calculados
				  */
				 for(int i =1;i < NumberLayer;i++)
				 {
					 for(int k = 0;k < NumberNeuronsForLayer[i];k++)
					 {


						 neuron[i][k].UpdateBias(LearningRatio,NeuronError[i][k]);
						 if(i==1)
						 {
							 for(int l = 0;l < 11;l++)
							 {
								 connection[0][l][k].UpdateWeightValue(LearningRatio,NeuronError[i][k],
										 neuron[0][l].GetResultActivationFunction());

							 }
						 }
						 else
						 {
							 for(int j = 0;j < NumberNeuronsForLayer[i-1];j++)
							 {
								connection[i-1][j][k].UpdateWeightValue(LearningRatio,NeuronError[i][k],
										neuron[i-1][j].GetResultActivationFunction());
							 }

						 }

					 }

				 }

				 /*
				  * Calculamos el error para este patron
				  */
				 error = error + (pow((stold(ArrayStudent[n].GetResult()) - neuron[NumberLayer - 1][0].GetResultActivationFunction()),2.0));
				 n++;
			}

			/*
			 * Calculamos el error total de la etapa de entranamiento
			 */
			totalerror = (1.0/100.0) * (0.5 * error);
			cout<<"cliclo " << CountLearningCycle << " Error " << totalerror << endl;
			LearningRatio = LearningRatio + 0.00001;

			cout << CountCross << endl;
			textinsert = "";

			/*
			  * Iniciamos Cross Validation
			  */
			int nSubset;
			int CountSubset = 0;

			switch(CountCross){
				case 0:
					CountSubset = 0;
					break;
				case 1:
					CountSubset = 20;
					break;
				case 2:
					CountSubset = 40;
					break;
				case 3:
					CountSubset = 60;
					break;
				case 4:
					CountSubset = 80;
					break;
				case 5:
					CountSubset = 100;
					break;
			}
			nSubset = CountSubset + 20;

			while(CountSubset <	 nSubset)
			{
				cout<< " count sub set " << CountSubset<<endl;
				auxmu=0;
				/*
				 * Las neuronas de entrada no tiene una función de activación por ello su resultado son los valores
				 * recolectados de los estudiantes.
				 */
				neuron[0][0].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex1()));
				neuron[0][1].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex2()));
				neuron[0][2].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex3()));
				neuron[0][3].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex4()));
				neuron[0][4].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex5()));
				neuron[0][5].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex6()));
				neuron[0][6].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex7()));
				neuron[0][7].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex8()));
				neuron[0][8].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex9()));
				neuron[0][9].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex10()));
				neuron[0][10].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex11()));

				for(int i =1;i < NumberLayer;i++)
				{
					for(int k = 0;k < NumberNeuronsForLayer[i];k++)
					{
						auxmu =0;
						/*
						 * Esta condicional realiza la inspeccion si en el recorrido i, se trata de una neuorna de la
						 * capa oculta numero 1, en este caso esta neurona esta conectada a la capa anterior por medio
						 * de las conexiones de la capa de entrada
						 */
						if(i==1)
						{

							for(int l = 0;l < 11;l++)
							{
								/*
								 * Sumatoria de entrada efectiva de cada neurona
								 */
								auxmu = auxmu + (neuron[0][l].GetResultActivationFunction()
										*  connection[0][l][k].GetWeightValue());
							}
							auxmu = auxmu + neuron[i][k].GetBias();

							/*
							 * Establecemos la sumatoria de entrada efectiva a su respectiva neurona
							 */
							neuron[i][k].SetMU(auxmu);

							/*
							 * Invocamos a la función, que operara la función de activación en la neurona
							 * con la entrada efectiva anteriormente establecida
							 */
							neuron[i][k].StartActivateFunction();
						}
						else
						{
							for(int j = 0;j < NumberNeuronsForLayer[i-1];j++)
							{
								/*
								 * Sumatoria de entrada efectiva de cada neurona
								 */
								auxmu = auxmu + (neuron[i-1][j].GetResultActivationFunction() *
										connection[i-1][j][k].GetWeightValue());

							}

							auxmu = auxmu + neuron[i][k].GetBias();
							/*
							 * Establecemos la sumatoria de entrada efectiva a su respectiva neurona
							 */
							neuron[i][k].SetMU(auxmu);

							/*
							 * Invocamos a la función, que operara la función de activación en la neurona
							 * con la entrada efectiva anteriormente establecida
							 */
							neuron[i][k].StartActivateFunction();
						}
					}

				}
				cout<< "salida " <<CountSubset+1 << " " << neuron[NumberLayer - 1][0].GetResultActivationFunction() << endl;
				MSE = MSE + pow((stold(ArrayStudent[CountSubset].GetResult()) - neuron[NumberLayer - 1][0].GetResultActivationFunction()),2);
				CountSubset++;
			}
			textinsert = "";
			ErrorCross = (1.0/20.0) * (0.5*MSE);
			cout<<"************************************************************"<<endl;
			cout<< "En ciclo de cross validation " << CountCross+1 << " error MSE: " << ErrorCross << endl;
			cout<<"************************************************************"<<endl;
			textinsert = textinsert + "insert into ResultBackpropagation values(" + to_string(CountCross + 1) +  ", "  + to_string(CountLearningCycle) + ", " + to_string(totalerror)  +  ", "  + to_string(ErrorCross) + ", '" + function + "');";
			sql = textinsert.c_str();
			/*
			 * Invocamos a la función de insertar
			 */
			resultDB = database.InsertResult(sql);
			if(resultDB == true)cout<<"Records created successfully"<<endl;
			else cout<<"Error while registering the date"<<endl;

			CountLearningCycle++;
		}
       CountCross++;
	}

	for(int j = 1;j < NumberLayer;j++)
	{
		for(int k = 0;k < NumberNeuronsForLayer[j];k++)
		{
			cout << "De la neurona " << j << "-"<< k << " sus umbrales son: " << neuron[j][k].GetBias() << endl;
		}
	}
	return 0;
}


