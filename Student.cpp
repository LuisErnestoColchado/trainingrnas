#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

class Student{

private:

	string cod;
	string name;
	string age;
	string index1;
	string index2;
	string index3;
	string index4;
	string index5;
	string index6;
	string index7;
	string index8;
	string index9;
	string index10;
	string index11;
	string result;

	string cluster;

public:

	string GetCod();
	void SetCod(string);

	string GetName();
	void SetName(string);

	string GetAge();
	void SetAge(string);

	string GetIndex1();
	void SetIndex1(string);

	string GetIndex2();
	void SetIndex2(string);

	string GetIndex3();
	void SetIndex3(string);

	string GetIndex4();
	void SetIndex4(string);

	string GetIndex5();
	void SetIndex5(string);

	string GetIndex6();
	void SetIndex6(string);

	string GetIndex7();
	void SetIndex7(string);

	string GetIndex8();
	void SetIndex8(string);

	string GetIndex9();
	void SetIndex9(string);

	string GetIndex10();
	void SetIndex10(string);

	string GetIndex11();
	void SetIndex11(string);

	string GetResult();
	void SetResult(string);

	string GetCluster();
	void SetCluster(string);


};

string Student::GetCod(){
	return cod;
}
void Student::SetCod(string pCod){
	cod = pCod;
}

string Student::GetName(){
	return name;
}
void Student::SetName(string pName){
	name = pName;
}

string Student::GetAge(){
	return age;
}
void Student::SetAge(string pAge){
	age = pAge;
}

string Student::GetIndex1(){
	return index1;
}
void Student::SetIndex1(string pIndex1){
	index1 = pIndex1;
}

string Student::GetIndex2(){
	return index2;
}
void Student::SetIndex2(string pIndex2){
	index2 = pIndex2;
}

string Student::GetIndex3(){
	return index3;
}
void Student::SetIndex3(string pIndex3){
	index3 = pIndex3;
}

string Student::GetIndex4(){
	return index4;
}
void Student::SetIndex4(string pIndex4){
	index4 = pIndex4;
}

string Student::GetIndex5(){
	return index5;
}
void Student::SetIndex5(string pIndex5){
	index5 = pIndex5;
}

string Student::GetIndex6(){
	return index6;
}
void Student::SetIndex6(string pIndex6){
	index6 = pIndex6;
}

string Student::GetIndex7(){
	return index6;
}
void Student::SetIndex7(string pIndex7){
	index7 = pIndex7;
}

string Student::GetIndex8(){
	return index8;
}
void Student::SetIndex8(string pIndex8){
	index8 = pIndex8;
}

string Student::GetIndex9(){
	return index9;
}
void Student::SetIndex9(string pIndex9){
	index9 = pIndex9;
}

string Student::GetIndex10(){
	return index10;
}
void Student::SetIndex10(string pIndex10){
	index10 = pIndex10;
}

string Student::GetIndex11(){
	return index11;
}
void Student::SetIndex11(string pIndex11){
	index11 = pIndex11;
}

string Student::GetResult(){
	return result;
}
void Student::SetResult(string pResult){
	result = pResult;
}

string Student::GetCluster(){
	return cluster;
}
void Student::SetCluster(string pCluster){
	cluster = pCluster;
}

